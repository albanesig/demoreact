import React, { Component } from 'react';
import './App.css';

import User from './User/User';

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      users: [],
      value: '',
      showUsers : true
    };
  }

  toggleUsers = () => {
    const doesShow = this.state.showUsers;
    this.setState( { showUsers: !doesShow } );
  };

  onAddUser = () => {
    this.setState( state => {
      const newUserList = [...state.users, state.value];
      return {
        newUserList,
        value: ''
      }
    })
  };

  onChangeValue = event => {
    this.setState({ value: event.target.value });
  };

  render() {

    let userList = null;

    if(this.state.showUsers) {
      userList = (
        <div>
        {this.state.users.map((user,index) => {
            return<User
              username={user.toString()}
              key={index}
            />
          }
        )}
        </div>
      )
    }

    return (
      <div className="App">
        <h2>User list</h2>
        <input type="text"
               onChange={this.onChangeValue}/>

               <button onClick={this.onAddUser}
                type="button">add</button>
        <br/>
        <button onClick={this.toggleUsers}>Toggle users</button>
        {userList}
      </div>
    )

  }
}

export default App;
