import React from 'react';

import './User.css'

const user = (props) => {

  return (
    <div className="User">
      <p>{props.username}</p>
    </div>
  )
};

export default user;